package ru.kpfu.itis.csport.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.kpfu.itis.csport")
public class CoreConfig {
}
